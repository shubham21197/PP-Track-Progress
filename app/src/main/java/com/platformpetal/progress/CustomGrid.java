package com.platformpetal.progress;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CustomGrid extends BaseAdapter{
    private Context mContext;
    private final String[] web;
    private final int[] Imageid;
    private final int[] colors;

    public CustomGrid(Context c,String[] web,int[] Imageid, int[] cols ) {
        mContext = c;
        this.Imageid = Imageid;
        this.web = web;
        this.colors = cols;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return web.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.grid_single, null);
            TextView textView = (TextView) grid.findViewById(R.id.grid_text);
            TextView progText = (TextView) grid.findViewById(R.id.progText);
            textView.setText(web[position]);
            ProgressBar mprogressBar = (ProgressBar) grid.findViewById(R.id.grid_image);
            progText.setText(Imageid[position] + "%");
            progText.setTextColor(colors[position]);
            mprogressBar.setMax(10000);
            mprogressBar.getProgressDrawable().setColorFilter(colors[position], PorterDuff.Mode.SRC_IN);
            ObjectAnimator anim = ObjectAnimator.ofInt(mprogressBar, "progress", 0, Imageid[position]*100);
            anim.setDuration(900);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.start();
        } else {
            grid = (View) convertView;
        }

        return grid;
    }
}