package com.platformpetal.progress;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class SubTask extends Activity {
    GridView grid;
    String[] text = {
            "Google",
            "Github",
            "Instagram",
            "Facebook",
            "Flickr",
            "Pinterest",
            "Quora",
            "Twitter"

    } ;
    int[] prog = {
            10,
            20,
            30,
            40,
            60,
            80,
            100,
            24

    };

    int[] cols = {
            Color.BLUE,
            Color.BLACK,
            Color.RED,
            Color.DKGRAY,
            Color.GREEN,
            Color.YELLOW,
            Color.MAGENTA,
            Color.CYAN


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_subtask);

        CustomGrid adapter = new CustomGrid(SubTask.this, text, prog, cols);
        grid=(GridView)findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(SubTask.this, "You Clicked at " +text[+ position], Toast.LENGTH_SHORT).show();
                Intent loadTask = new Intent(SubTask.this, TaskLoader.class);
                loadTask.putExtra("header", text[position]);
                loadTask.putExtra("color", cols[position]);
                loadTask.putExtra("progress", prog[position]);
                startActivity(loadTask);

            }
        });

    }

}