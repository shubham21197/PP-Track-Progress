package com.platformpetal.progress;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MainActivity extends Activity {
    GridView grid;
    int[] prog = {
            10,
            20,
            30,
            40,
            60,
            80,
            100,
            24

    };

    int[] cols = {
            Color.RED


    };

    String[] text = {
            "Complete"
    };

    public int[] avg(int[] abc){
        int cnt=0,sum=0;
        for(int i=0; i<abc.length; i++){
            sum+=abc[i];
            cnt=i;
        }
        sum=sum/cnt;
        int[] average = {sum};
        return average;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomGrid adapter = new CustomGrid(MainActivity.this, text, avg(prog), cols);
        grid=(GridView)findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                startActivity(new Intent(MainActivity.this, SubTask.class));

            }
        });

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                String cmd = "ssh ";
                Process process = Runtime.getRuntime().exec(cmd);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                StringBuilder log = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()).length() > -1)
                {
                    Toast.makeText(MainActivity.this, line, Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception e){}}
        });

    }

}