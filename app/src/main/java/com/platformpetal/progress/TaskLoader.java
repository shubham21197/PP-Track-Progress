package com.platformpetal.progress;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by mahe on 13-07-2017.
 */

public class TaskLoader extends Activity {

    ListView list;
    String[] web = {
            "Shubham",
            "Rishi",
            "Bhaskar",
            "Divyanshu",
            "Fanny Boi"
    } ;

    String[] web_desc = {
            "Shubham",
            "Rishi",
            "Bhaskar",
            "Divyanshu",
            "Fanny Boi"
    } ;

    Integer[] imageId = {
            R.drawable.image,
            R.drawable.image,
            R.drawable.image,
            R.drawable.image,
            R.drawable.image

    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskloader_activity);

        Bundle extras = getIntent().getExtras();
        String header = extras.getString("header");
        int color = extras.getInt("color");
        int progress = extras.getInt("progress");

        View headerView = getLayoutInflater().inflate(R.layout.member_list_header, null, false);

        CustomListMember adapter = new
                CustomListMember(this, web, web_desc, imageId);


        list=(ListView)findViewById(R.id.task_member);
        list.addHeaderView(headerView);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(TaskLoader.this, "You Clicked at " +web[+ position -1], Toast.LENGTH_SHORT).show();

            }
        });

        TextView textView = (TextView) findViewById(R.id.task_head);
        TextView progText = (TextView) findViewById(R.id.task_perc);
        textView.setText(header);
        ProgressBar mprogressBar = (ProgressBar) findViewById(R.id.task_prog);
        int width = getResources().getDisplayMetrics().widthPixels;
        width= (int)(width*0.85);
        android.view.ViewGroup.LayoutParams mParams = mprogressBar.getLayoutParams();
        mParams.height = width;
        mprogressBar.setLayoutParams(mParams);
        progText.setText(progress + "%");
        progText.setTextColor(color);
        //mprogressBar.setMax(10000);
        mprogressBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        final ObjectAnimator anim = ObjectAnimator.ofInt(mprogressBar, "progress", 0, progress*100);
        final ObjectAnimator anim_text = ObjectAnimator.ofFloat(progText, "alpha", 0, 1f);
        anim_text.setDuration(900);
        anim.setDuration(900);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();
        anim_text.start();
        mprogressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anim.start();
                anim_text.start();
            }
        });




    }

}
