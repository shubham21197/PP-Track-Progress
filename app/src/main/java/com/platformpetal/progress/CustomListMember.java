package com.platformpetal.progress;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomListMember extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    private final String[] web_desc;
    public CustomListMember(Activity context,
                      String[] web, String web_desc[], Integer[] imageId) {
        super(context, R.layout.member_listitem, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;
        this.web_desc = web_desc;

    }

    public int getCount(){
        return web.length;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.member_listitem, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.member_name);
        TextView txtDesc = (TextView) rowView.findViewById(R.id.member_desc);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.member_pic);

        txtTitle.setText(web[position]);
        txtDesc.setText(web_desc[position]);

        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}
